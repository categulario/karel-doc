Karel el robot
==============

.. toctree::
   :maxdepth: 2
   :caption: Contenido:

Aprende a usar karel el robot. Karel es la flecha azul que apunta hacia tu destino.

Instrucciones básicas
---------------------

.. glossary::

   avanza
      Mueve a karel una casilla hacia adelante en la dirección en la que apunta la flecha. Falla si hay una pared enfrente.

   gira-izquierda
      Causa una rotación de 90° en sentido antihorario (a la izquierda) de Karel en la casilla actual.

   coge-zumbador
      Toma un zumbador de la casilla actual de Karel y lo deposita en su mochila. Falla si no hay zumbadores en esa casilla.

   deja-zumbador
      Deja un zumbador de la mochila de Karel en la casilla actual. Falla si no hay ningún zumbador en la mochila.

   apágate
      Termina la ejecución del programa actual sin importar si existen instrucciones futuras.

Estructuras de Control
----------------------

Las estructuras de control son instrucciones que indican al programa la ejecución de bloques de código condicionales, repetitivos o con nombre.

.. todo::
   Hablar sobre qué es un bloque.

.. glossary::

   Bloque condicional
      Permite la ejecución condicional de un bloque de código dependiendo de si se cumple una condición o no.

A continuación un código que solo recoge un zumbador si éste existe en la casilla actual:

.. code-block:: text

   si junto-a-zumbador
      coge-zumbador
   fin

También es posible indicar un bloque de instrucciones en caso de que la condición no se cumpla usando un bloque ``sino``. En el siguiente ejemplo Karel girará a la izquierda si tiene una pared enfrente.

.. code-block:: text

   si frente-libre
      avanza
   sino
      gira-izquierda
   fin

.. glossary::

   Bucle simple (for)
      Repite instrucciones una cantidad específica de veces dada por una variable

En el siguiente ejemplo se usa un **bucle simple** para repetir la instrucción **avanza** 5 veces.

.. code-block:: text

   5 veces
      avanza
   fin

.. glossary::

   Bucle condicional
      Repite instrucciones mientras se cumpla la condición dada

En el siguiente ejemplo se usa un **bucle condicional** para mover a karel hasta la siguiente pared.

.. code-block:: text

   mientras frente-libre
      avanza
   fin

.. glossary::

   Definición de funciones
      Permite ponerle nombre (y posiblemente argumentos) a un bloque de código que después puede ser utilizado con ese nombre.

Esto facilita la reutilización de bloques de código muy frecuentes. En el siguiente ejemplo se define la instrucción ``gira-derecha`` y se utiliza para mover a karel a la siguiente casilla.

.. code-block:: text

   def gira-derecha
      3 veces
         gira-izquierda
      fin
   fin

   gira-derecha
   avanza

Índices y tablas
================

* :ref:`genindex`
* :ref:`search`
